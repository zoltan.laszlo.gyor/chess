package com.game.pieces;

import com.game.table.Field;
import com.game.table.Table;

import java.util.ArrayList;

public class Pawn extends ChessPiece implements ChessPieceActions {

    public Pawn(int row, int column, Color color){
        super(row, column, color);
    }

    @Override
    public void move(int row, int column) {

    }

    @Override
    public ArrayList<Field> possibleMoves(Table table) {
        return null;
    }

    @Override
    public String toString(){
       return "P";
    }
}
