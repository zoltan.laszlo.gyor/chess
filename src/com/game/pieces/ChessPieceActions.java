package com.game.pieces;

import com.game.table.Field;
import com.game.table.Table;

import java.util.ArrayList;

public interface ChessPieceActions {
    public void move(int row, int column);

    public ArrayList<Field> possibleMoves(Table table);
}
